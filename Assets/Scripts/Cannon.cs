﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour
{

	private Rigidbody2D rb;
	public GameObject shifterPrefab;
	public Animator animator;
	public AudioSource launchSound;

	private float angle;
	public float shiftersFlipPower = 200;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		Vector3 mouse = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));

		Vector3 dir = mouse - transform.position;
		angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		if (Input.GetMouseButtonDown(0))
		{
			CreateShifter();
		}
	}

	private void CreateShifter()
	{
		launchSound.Play();
		Vector3 pos = transform.position;
		pos.z += 0.01f;
		GameObject shifterGobj = (GameObject)GameObject.Instantiate(shifterPrefab, pos, Quaternion.identity);
		Shifter shifter = shifterGobj.GetComponent<Shifter>();
		shifter.shiftersFlipPower = shiftersFlipPower;
		shifterGobj.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
		animator.SetTrigger("Fire");
	}
}
