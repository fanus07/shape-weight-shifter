using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Shape: MonoBehaviour
{
	private Rigidbody2D rb;
	public List<GameObject> buttons;
	public GameObject endCanvas;
	public GameObject cannon;
	public Animator anim;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(buttons.Count <= 0)
			if(collision.collider.gameObject.layer == 8)
			{
				EndLevel();
			}
	}
	public void CheckEndLevel(Collision2D collision)
	{
		int missingCount = 0;
		foreach(GameObject gobj in buttons)
		{
			if(gobj == null)
			{
				missingCount++;
			}
		}

		if(missingCount == buttons.Count - 1)
			EndLevel();
	}
	public void EndLevel()
	{
		anim.SetTrigger("Win");
		cannon.SetActive(false);
		endCanvas.SetActive(true);
	}
}
