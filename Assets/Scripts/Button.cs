using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D collision)
	{
		GameObject parent = transform.parent.gameObject;
		Shape shape = parent.GetComponent<Shape>();

		shape.CheckEndLevel(collision);

		if (collision.collider.gameObject.layer == 8)
			Destroy(gameObject);

	}
}
