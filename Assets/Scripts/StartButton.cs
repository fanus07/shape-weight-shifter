﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartButton : MonoBehaviour {
	public string loadScene;
	public AudioSource clickSound;

	public void OnClick () {
		clickSound.Play();
		StartCoroutine(WaitThenLoad());
	}
	private IEnumerator WaitThenLoad()
	{
		yield return new WaitForSeconds(0.1f);
		if(loadScene == "Current")
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);	
		else
			SceneManager.LoadScene(loadScene);	
	}
}
