using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shifter : MonoBehaviour
{
	public GameObject torso;
	private Rigidbody2D rb;
	public Animator anim;
	public ParticleSystem particleSystem;
	public Color collisionParticleColor;

	public float shiftersFlipPower = 200;

	void Start()
	{
		rb = torso.GetComponent<Rigidbody2D>();
		rb.AddForce(transform.up * 3000);
		rb.AddTorque(shiftersFlipPower);
	}
}
