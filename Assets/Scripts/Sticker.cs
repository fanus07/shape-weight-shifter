using UnityEngine;
using System.Collections;

public class Sticker : MonoBehaviour
{
	private Rigidbody2D rb;
	private int stickCount;
	public int stickLimit;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		/* if(transform.localPosition.y > 100 || transform.localPosition.y < 100 || transform.localPosition.x > 100 || transform.localPosition.x < 100) */
		/* 	Destroy(gameObject); */
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(stickCount < stickLimit)
			/* if (gameObject.GetComponent<HingeJoint2D>() == null) */
			/* { */
			if(collision.collider.gameObject.layer != 8)
			{
				if (collision.collider.transform.parent == null || (collision.collider.transform.parent != null && collision.collider.transform.parent.gameObject != gameObject.transform.parent.gameObject && collision.collider.gameObject.GetComponent<FixedJoint>() == null))
				{
					stickCount++;
					/* HingeJoint2D hingeJoint = gameObject.AddComponent<HingeJoint2D>(); */
					/* hingeJoint.autoConfigureConnectedAnchor = true; */
					/* hingeJoint.connectedBody = collision.collider.GetComponent<Rigidbody2D>(); */
					/* hingeJoint.useLimits = true; */
					/* JointAngleLimits2D limits = new JointAngleLimits2D(); */
					/* limits.max = 20; */
					/* limits.min = -20; */
					/* hingeJoint.limits = limits; */
					/* hingeJoint.anchor = collision.contacts[0].point; */
					// get parent set animtriggergcc
					Shifter shifter = gameObject.transform.parent.gameObject.GetComponent<Shifter>();
					shifter.anim.SetTrigger("Sticking");
					shifter.particleSystem.startColor = shifter.collisionParticleColor;
					ParticleSystem.ShapeModule shapeModule = shifter.particleSystem.shape;
					shapeModule.randomDirection = true;
					FixedJoint2D fixedJoint = gameObject.AddComponent<FixedJoint2D>();
					fixedJoint.autoConfigureConnectedAnchor = true;
					fixedJoint.connectedBody = collision.collider.GetComponent<Rigidbody2D>();
					fixedJoint.enabled = true;
					/* fixedJoint.anchor = collision.contacts[0].point; */
				}
			}
		/* } */
	}
}
