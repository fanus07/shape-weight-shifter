using UnityEngine;
using System.Collections;

public class Void : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.transform.parent != null && collision.collider.transform.parent.gameObject.layer == 11)
			Destroy(collision.collider.transform.parent.gameObject);
	}
}
